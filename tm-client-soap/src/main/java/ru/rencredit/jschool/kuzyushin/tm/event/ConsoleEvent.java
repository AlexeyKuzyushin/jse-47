package ru.rencredit.jschool.kuzyushin.tm.event;

import org.springframework.context.ApplicationEvent;
import org.springframework.stereotype.Component;
import ru.rencredit.jschool.kuzyushin.tm.boostrap.ClientBootstrap;

@Component
public class ConsoleEvent extends ApplicationEvent {

    public ConsoleEvent(ClientBootstrap bootstrap) {
        super(bootstrap);
    }

    private String command;

    public String getCommand() {
        return command;
    }

    public void setCommand(String command) {
        this.command = command;
    }
}
