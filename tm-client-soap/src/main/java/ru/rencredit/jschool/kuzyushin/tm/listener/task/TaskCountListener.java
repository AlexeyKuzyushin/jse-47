package ru.rencredit.jschool.kuzyushin.tm.listener.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.rencredit.jschool.kuzyushin.tm.endpoint.soap.TaskSoapEndpoint;
import ru.rencredit.jschool.kuzyushin.tm.event.ConsoleEvent;
import ru.rencredit.jschool.kuzyushin.tm.listener.AbstractListener;
import ru.rencredit.jschool.kuzyushin.tm.service.SessionService;

@Component
public final class TaskCountListener extends AbstractListener {

    @NotNull
    private final TaskSoapEndpoint taskSoapEndpoint;

    @NotNull
    private final SessionService sessionService;

    @Autowired
    public TaskCountListener(
            final @NotNull TaskSoapEndpoint taskSoapEndpoint,
            final @NotNull SessionService sessionService
    ) {
        this.taskSoapEndpoint = taskSoapEndpoint;
        this.sessionService = sessionService;
    }

    @NotNull
    @Override
    public String name() {
        return "task-count";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Count all tasks";
    }

    @Override
    @EventListener(condition = "@taskCountListener.name() == #event.command")
    public void handler(final ConsoleEvent event) {
        System.out.println("[COUNT TASKS]");
        sessionService.setListCookieRowRequest(taskSoapEndpoint);
        System.out.println("COUNT: " + taskSoapEndpoint.countAllTasks());
        System.out.println("[OK]");
    }
}
