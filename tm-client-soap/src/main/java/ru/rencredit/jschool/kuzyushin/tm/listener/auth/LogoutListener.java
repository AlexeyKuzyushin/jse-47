package ru.rencredit.jschool.kuzyushin.tm.listener.auth;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.rencredit.jschool.kuzyushin.tm.endpoint.soap.AuthSoapEndpoint;
import ru.rencredit.jschool.kuzyushin.tm.event.ConsoleEvent;
import ru.rencredit.jschool.kuzyushin.tm.listener.AbstractListener;
import ru.rencredit.jschool.kuzyushin.tm.service.SessionService;

import java.util.ArrayList;

@Component
public final class LogoutListener extends AbstractListener {

    @NotNull
    private final AuthSoapEndpoint authSoapEndpoint;

    @NotNull
    private final SessionService sessionService;

    @Autowired
    public LogoutListener(
            final @NotNull AuthSoapEndpoint authSoapEndpoint,
            final @NotNull SessionService sessionService
    ) {
        this.authSoapEndpoint = authSoapEndpoint;
        this.sessionService = sessionService;
    }

    @NotNull
    @Override
    public String name() {
        return "logout";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Logout";
    }

    @Override
    @EventListener(condition = "@logoutListener.name() == #event.command")
    public void handler(final ConsoleEvent event) {
        System.out.println("[LOGOUT]");
        authSoapEndpoint.logout();
        sessionService.setCookieHeaders(new ArrayList<>());
        System.out.println("[OK]");
    }
}
