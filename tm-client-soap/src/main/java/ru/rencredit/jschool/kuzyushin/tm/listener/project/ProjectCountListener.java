package ru.rencredit.jschool.kuzyushin.tm.listener.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.rencredit.jschool.kuzyushin.tm.endpoint.soap.ProjectSoapEndpoint;
import ru.rencredit.jschool.kuzyushin.tm.event.ConsoleEvent;
import ru.rencredit.jschool.kuzyushin.tm.listener.AbstractListener;
import ru.rencredit.jschool.kuzyushin.tm.service.SessionService;

@Component
public final class ProjectCountListener extends AbstractListener {

    @NotNull
    private final ProjectSoapEndpoint projectSoapEndpoint;

    @NotNull
    private final SessionService sessionService;

    @Autowired
    public ProjectCountListener(
            final @NotNull ProjectSoapEndpoint projectSoapEndpoint,
            final @NotNull SessionService sessionService
    ) {
        this.projectSoapEndpoint = projectSoapEndpoint;
        this.sessionService = sessionService;
    }

    @NotNull
    @Override
    public String name() {
        return "project-count";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Count all projects";
    }

    @Override
    @EventListener(condition = "@projectCountListener.name() == #event.command")
    public void handler(final ConsoleEvent event) {
        System.out.println("[COUNT PROJECTS]");
        sessionService.setListCookieRowRequest(projectSoapEndpoint);
        System.out.println("COUNT: " + projectSoapEndpoint.countAllProjects());
        System.out.println("[OK]");
    }
}
