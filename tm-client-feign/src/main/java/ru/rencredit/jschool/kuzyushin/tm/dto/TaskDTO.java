package ru.rencredit.jschool.kuzyushin.tm.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
public class TaskDTO extends AbstractEntityDTO{

    public static final long serialVersionUID = 1;

    @NotNull
    private String name;

    @Nullable
    private String description;

    private String userId;

    private String projectId;

    @Nullable
    private Date startDate;

    @Nullable
    private Date finishDate;

    @Nullable
    private Date creationDate;

    @Override
    public String toString() {
        return getId() + ": " + name;
    }
}
