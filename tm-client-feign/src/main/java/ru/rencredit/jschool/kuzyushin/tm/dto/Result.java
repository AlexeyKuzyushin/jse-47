package ru.rencredit.jschool.kuzyushin.tm.dto;

public class Result {

    public boolean success = true;

    public String message = "";

    public Result(Boolean success) {this.success = success;}

    public Result() {
    }
}
