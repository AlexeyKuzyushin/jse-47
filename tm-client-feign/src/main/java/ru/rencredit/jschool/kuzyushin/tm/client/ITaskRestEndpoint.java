package ru.rencredit.jschool.kuzyushin.tm.client;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import ru.rencredit.jschool.kuzyushin.tm.dto.TaskDTO;

import java.util.List;

@FeignClient(name = "tm-server")
@RequestMapping(value="/rest/tasks", produces = MediaType.APPLICATION_JSON_VALUE)
public interface ITaskRestEndpoint {

    @PostMapping(value = "/create")
    public TaskDTO createTask(@RequestBody final @Nullable TaskDTO taskDTO);

    @PutMapping(value = "/update")
    public TaskDTO updateTask(@RequestBody final @Nullable TaskDTO taskDTO);

    @NotNull
    @GetMapping(value = "/count")
    public Long countOfTasks();

    @Nullable
    @GetMapping(value = "/find/{id}")
    public TaskDTO findTask(@PathVariable("id") final @Nullable String id);

    @NotNull
    @GetMapping(value = "/findAll")
    public List<TaskDTO> findAllTasks();

    @DeleteMapping(value = "/remove/{id}")
    public void deleteTask(@PathVariable("id") final @Nullable String id);
}
