package ru.rencredit.jschool.kuzyushin.tm.client;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import ru.rencredit.jschool.kuzyushin.tm.dto.ProjectDTO;

import java.util.List;

@FeignClient(name = "tm-server")
@RequestMapping(value = "/rest/projects", produces = MediaType.APPLICATION_JSON_VALUE)
public interface IProjectRestEndpoint {

    @PostMapping(value = "/create")
    public ProjectDTO createProject(@RequestBody final @Nullable ProjectDTO projectDTO);

    @PutMapping(value = "/update")
    public ProjectDTO updateProject(@RequestBody final @Nullable  ProjectDTO projectDTO);

    @NotNull
    @GetMapping(value = "/count")
    public Long countOfProjects();

    @Nullable
    @GetMapping(value = "/find/{id}")
    public ProjectDTO findProject(@PathVariable("id") final @Nullable String id);

    @NotNull
    @GetMapping(value = "/findAll")
    public List<ProjectDTO> findAllProjects();

    @DeleteMapping(value = "/remove/{id}")
    public void deleteProject(@PathVariable("id") final @Nullable String id);
}
