package ru.rencredit.jschool.kuzyushin.tm.enumeration;

import org.jetbrains.annotations.NotNull;

public enum RoleType {

    USER("Пользователь"),
    ADMIN("Администратор");

    @NotNull
    private final String displayName;

    RoleType(@NotNull String displayName) {
        this.displayName = displayName;
    }

    @NotNull
    public String getDisplayName() {
        return displayName;
    }
}
