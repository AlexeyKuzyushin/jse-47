package ru.rencredit.jschool.kuzyushin.tm;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;

@EnableZuulProxy
@SpringBootApplication
public class TmProxyServerApplication {

	public static void main(String[] args) {
		SpringApplication.run(TmProxyServerApplication.class, args);
	}

}
