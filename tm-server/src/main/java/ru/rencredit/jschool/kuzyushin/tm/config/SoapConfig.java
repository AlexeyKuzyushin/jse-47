package ru.rencredit.jschool.kuzyushin.tm.config;

import org.apache.cxf.Bus;
import org.apache.cxf.jaxws.EndpointImpl;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import ru.rencredit.jschool.kuzyushin.tm.endpoint.soap.AuthSoapEndpoint;
import ru.rencredit.jschool.kuzyushin.tm.endpoint.soap.ProjectSoapEndpoint;
import ru.rencredit.jschool.kuzyushin.tm.endpoint.soap.TaskSoapEndpoint;

import javax.xml.ws.Endpoint;

/**
 *  http://localhost:8080/services/ProjectEndpoint?wsdl
 *  http://localhost:8080/services/TaskEndpoint?wsdl
 *  http://localhost:8080/services/AuthEndpoint?wsdl
 */

@Configuration
public class SoapConfig {

    @Bean
    public Endpoint projectEndpointRegistry(@NotNull final ProjectSoapEndpoint projectSoapEndpoint, @NotNull Bus bus) {
        final EndpointImpl endpoint = new EndpointImpl(bus, projectSoapEndpoint);
        endpoint.publish("/ProjectEndpoint");
        return endpoint;
    }

    @Bean
    public Endpoint taskEndpointRegistry(final TaskSoapEndpoint taskSoapEndpoint, @NotNull Bus bus) {
        final EndpointImpl endpoint = new EndpointImpl(bus, taskSoapEndpoint);
        endpoint.publish("/TaskEndpoint");
        return endpoint;
    }

    @Bean
    public Endpoint authEndpointRegistry(final AuthSoapEndpoint authSoapEndpoint, @NotNull Bus bus) {
        final EndpointImpl endpoint = new EndpointImpl(bus, authSoapEndpoint);
        endpoint.publish("/AuthEndpoint");
        return endpoint;
    }
}
