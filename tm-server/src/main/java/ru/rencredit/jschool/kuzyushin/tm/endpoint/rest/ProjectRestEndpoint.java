package ru.rencredit.jschool.kuzyushin.tm.endpoint.rest;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import ru.rencredit.jschool.kuzyushin.tm.api.service.IProjectService;
import ru.rencredit.jschool.kuzyushin.tm.dto.ProjectDTO;
import ru.rencredit.jschool.kuzyushin.tm.util.UserUtil;

import java.util.List;

@RestController
@RequestMapping(value = "/rest/projects", produces = MediaType.APPLICATION_JSON_VALUE)
public class ProjectRestEndpoint {

    @NotNull
    private final IProjectService projectService;

    @Autowired
    public ProjectRestEndpoint(final @NotNull IProjectService projectService
    ) {
        this.projectService = projectService;
    }

    @PostMapping(value = "/create", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ProjectDTO createProject(@RequestBody final @Nullable ProjectDTO projectDTO) {
        return ProjectDTO.toDTO(projectService.create(
                UserUtil.getUserId(),
                projectDTO.getName(),
                projectDTO.getDescription()));
    }

    @PutMapping(value = "/update")
    public ProjectDTO updateProject(@RequestBody final @Nullable  ProjectDTO projectDTO) {
        return ProjectDTO.toDTO(projectService.updateById(
                UserUtil.getUserId(),
                projectDTO.getId(),
                projectDTO.getName(),
                projectDTO.getDescription()));
    }

    @NotNull
    @GetMapping(value = "/count")
    public Long countOfProjects() {
        return projectService.count();
    }

    @Nullable
    @GetMapping(value = "/find/{id}")
    public ProjectDTO findProject(@PathVariable("id") final @Nullable String id) {
        return ProjectDTO.toDTO(projectService.findProjectById(id));
    }

    @NotNull
    @GetMapping(value = "/findAll")
    public List<ProjectDTO> findAllProjects() {
        return projectService.findAll();
    }

    @DeleteMapping(value = "/remove/{id}")
    public void deleteProject(@PathVariable("id") final @Nullable String id) {
        projectService.removeProjectById(id);
    }
}
