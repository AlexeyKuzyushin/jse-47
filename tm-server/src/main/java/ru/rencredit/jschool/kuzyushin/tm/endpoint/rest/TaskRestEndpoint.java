package ru.rencredit.jschool.kuzyushin.tm.endpoint.rest;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import ru.rencredit.jschool.kuzyushin.tm.api.service.ITaskService;
import ru.rencredit.jschool.kuzyushin.tm.dto.TaskDTO;
import ru.rencredit.jschool.kuzyushin.tm.util.UserUtil;

import java.util.List;

@RestController
@RequestMapping(value="/rest/tasks", produces = MediaType.APPLICATION_JSON_VALUE)
public class TaskRestEndpoint {

    private final ITaskService taskService;

    @Autowired
    public TaskRestEndpoint(final @NotNull ITaskService taskService) {
        this.taskService = taskService;
    }

    @PostMapping(value = "/create")
    public TaskDTO createTask(@RequestBody final @Nullable TaskDTO taskDTO) {
        return TaskDTO.toDTO(taskService.create(
                UserUtil.getUserId(),
                taskDTO.getProjectId(),
                taskDTO.getName(),
                taskDTO.getDescription()));
    }

    @PutMapping(value = "/update")
    public TaskDTO updateTask(@RequestBody final @Nullable TaskDTO taskDTO) {
        return TaskDTO.toDTO(taskService.updateById(
                UserUtil.getUserId(),
                taskDTO.getId(),
                taskDTO.getName(),
                taskDTO.getDescription()));
    }

    @NotNull
    @GetMapping(value = "/count")
    public Long countOfTasks() {
        return taskService.count();
    }

    @Nullable
    @GetMapping(value = "/find/{id}")
    public TaskDTO findTask(@PathVariable("id") final @Nullable String id) {
        return TaskDTO.toDTO(taskService.findTaskById(id));
    }

    @NotNull
    @GetMapping(value = "/findAll")
    public List<TaskDTO> findAllTasks() {
        return taskService.findAll();
    }

    @DeleteMapping(value = "/remove/{id}")
    public void deleteTask(@PathVariable("id") final @Nullable String id) {
        taskService.removeTaskById(id);
    }
}
