package ru.rencredit.jschool.kuzyushin.tm;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.config.server.EnableConfigServer;

@EnableConfigServer
@SpringBootApplication
public class TmServerConfigApplication {

	public static void main(String[] args) {
		SpringApplication.run(TmServerConfigApplication.class, args);
	}

}
